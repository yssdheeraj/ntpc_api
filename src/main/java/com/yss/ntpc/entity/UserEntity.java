package com.yss.ntpc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_userDetails")
public class UserEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -42675707433339754L;

	@Id
	@Column(name = "loginId")
	private String loginId;

	@Column(name = "userName")
	private String userName;

	@Column(name="user_type")
	private String userType;
	
	@Column(name = "password")
	private String password;

	@Column(name = "mobile")
	private String mobile;

	@Column(name = "emailId")
	private String emailId;
	
	@ManyToOne
	@JoinColumn(name = "location_id", nullable = false)
	private LocationEntity location;

	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "createDate")
	private String createDate;

   @Column(name="flag", columnDefinition = "boolean default false")
   private boolean flag;
	
	
	
	// @Column(name="value",columnDefinition = "boolean default false")

	@Column(name = "adduserMaster", columnDefinition = "boolean default false")
	private boolean adduserMasterView;

	@Column(name = "viewuserMaster", columnDefinition = "boolean default false")
	private boolean viewuserMasterView;

	@Column(name = "modifyuserMaster", columnDefinition = "boolean default false")
	private boolean modifyuserMasterView;

	@Column(name = "deleteuserMaster", columnDefinition = "boolean default false")
	private boolean deleteuserMasterView;

	@Column(name = "addmobileMaster", columnDefinition = "boolean default false")
	private boolean addmobileMasterView;

	@Column(name = "viewmobileMaster", columnDefinition = "boolean default false")
	private boolean viewmobileMasterView;

	@Column(name = "modifymobileMaster", columnDefinition = "boolean default false")
	private boolean modifymobileMasterView;

	@Column(name = "deletemobileMaster", columnDefinition = "boolean default false")
	private boolean deletemobileMasterView;

	@Column(name = "addphoneDirectory", columnDefinition = "boolean default false")
	private boolean addphoneDirectoryView;

	@Column(name = "viewphoneDirectory", columnDefinition = "boolean default false")
	private boolean viewphoneDirectoryView;

	@Column(name = "modifyphoneDirectory", columnDefinition = "boolean default false")
	private boolean modifyphoneDirectoryView;

	@Column(name = "deletephoneDirectory", columnDefinition = "boolean default false")
	private boolean deletephoneDirectoryView;

	@Column(name = "addcontactGroup", columnDefinition = "boolean default false")
	private boolean addcontactGroupView;

	@Column(name = "viewcontactGroup", columnDefinition = "boolean default false")
	private boolean viewcontactGroupView;

	@Column(name = "modifycontactGroup", columnDefinition = "boolean default false")
	private boolean modifycontactGroupView;

	@Column(name = "deletecontactGroup", columnDefinition = "boolean default false")
	private boolean deletecontactGroupView;

	@Column(name = "addsmsDraft", columnDefinition = "boolean default false")
	private boolean addsmsDraftView;

	@Column(name = "viewsmsDraft", columnDefinition = "boolean default false")
	private boolean viewsmsDraftView;

	@Column(name = "modifysmsDraft", columnDefinition = "boolean default false")
	private boolean modifysmsDraftView;

	@Column(name = "deletesmsDraft", columnDefinition = "boolean default false")
	private boolean deletesmsDraftView;

	@Column(name = "addinstantMessage", columnDefinition = "boolean default false")
	private boolean addinstantMessageView;

	@Column(name = "addscheduleMessage", columnDefinition = "boolean default false")
	private boolean addscheduleMessageView;

	@Column(name = "viewscheduleMessage", columnDefinition = "boolean default false")
	private boolean viewscheduleMessageView;

	@Column(name = "modifyscheduleMessage", columnDefinition = "boolean default false")
	private boolean modifyscheduleMessageView;

	@Column(name = "deletescheduleMessage", columnDefinition = "boolean default false")
	private boolean deletescheduleMessageView;

	@Column(name = "viewinstantMessageReport", columnDefinition = "boolean default false")
	private boolean viewinstantMessageReportView;

	@Column(name = "viewscheduleMessageReport", columnDefinition = "boolean default false")
	private boolean viewscheduleMessageReportView;

	public UserEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserEntity(String loginId, String userName, String userType, String password, String mobile, String emailId,
			LocationEntity location, String createdBy, String createDate, boolean flag, boolean adduserMasterView,
			boolean viewuserMasterView, boolean modifyuserMasterView, boolean deleteuserMasterView,
			boolean addmobileMasterView, boolean viewmobileMasterView, boolean modifymobileMasterView,
			boolean deletemobileMasterView, boolean addphoneDirectoryView, boolean viewphoneDirectoryView,
			boolean modifyphoneDirectoryView, boolean deletephoneDirectoryView, boolean addcontactGroupView,
			boolean viewcontactGroupView, boolean modifycontactGroupView, boolean deletecontactGroupView,
			boolean addsmsDraftView, boolean viewsmsDraftView, boolean modifysmsDraftView, boolean deletesmsDraftView,
			boolean addinstantMessageView, boolean addscheduleMessageView, boolean viewscheduleMessageView,
			boolean modifyscheduleMessageView, boolean deletescheduleMessageView, boolean viewinstantMessageReportView,
			boolean viewscheduleMessageReportView) {
		super();
		this.loginId = loginId;
		this.userName = userName;
		this.userType = userType;
		this.password = password;
		this.mobile = mobile;
		this.emailId = emailId;
		this.location = location;
		this.createdBy = createdBy;
		this.createDate = createDate;
		this.flag = flag;
		this.adduserMasterView = adduserMasterView;
		this.viewuserMasterView = viewuserMasterView;
		this.modifyuserMasterView = modifyuserMasterView;
		this.deleteuserMasterView = deleteuserMasterView;
		this.addmobileMasterView = addmobileMasterView;
		this.viewmobileMasterView = viewmobileMasterView;
		this.modifymobileMasterView = modifymobileMasterView;
		this.deletemobileMasterView = deletemobileMasterView;
		this.addphoneDirectoryView = addphoneDirectoryView;
		this.viewphoneDirectoryView = viewphoneDirectoryView;
		this.modifyphoneDirectoryView = modifyphoneDirectoryView;
		this.deletephoneDirectoryView = deletephoneDirectoryView;
		this.addcontactGroupView = addcontactGroupView;
		this.viewcontactGroupView = viewcontactGroupView;
		this.modifycontactGroupView = modifycontactGroupView;
		this.deletecontactGroupView = deletecontactGroupView;
		this.addsmsDraftView = addsmsDraftView;
		this.viewsmsDraftView = viewsmsDraftView;
		this.modifysmsDraftView = modifysmsDraftView;
		this.deletesmsDraftView = deletesmsDraftView;
		this.addinstantMessageView = addinstantMessageView;
		this.addscheduleMessageView = addscheduleMessageView;
		this.viewscheduleMessageView = viewscheduleMessageView;
		this.modifyscheduleMessageView = modifyscheduleMessageView;
		this.deletescheduleMessageView = deletescheduleMessageView;
		this.viewinstantMessageReportView = viewinstantMessageReportView;
		this.viewscheduleMessageReportView = viewscheduleMessageReportView;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public LocationEntity getLocation() {
		return location;
	}

	public void setLocation(LocationEntity location) {
		this.location = location;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public boolean isAdduserMasterView() {
		return adduserMasterView;
	}

	public void setAdduserMasterView(boolean adduserMasterView) {
		this.adduserMasterView = adduserMasterView;
	}

	public boolean isViewuserMasterView() {
		return viewuserMasterView;
	}

	public void setViewuserMasterView(boolean viewuserMasterView) {
		this.viewuserMasterView = viewuserMasterView;
	}

	public boolean isModifyuserMasterView() {
		return modifyuserMasterView;
	}

	public void setModifyuserMasterView(boolean modifyuserMasterView) {
		this.modifyuserMasterView = modifyuserMasterView;
	}

	public boolean isDeleteuserMasterView() {
		return deleteuserMasterView;
	}

	public void setDeleteuserMasterView(boolean deleteuserMasterView) {
		this.deleteuserMasterView = deleteuserMasterView;
	}

	public boolean isAddmobileMasterView() {
		return addmobileMasterView;
	}

	public void setAddmobileMasterView(boolean addmobileMasterView) {
		this.addmobileMasterView = addmobileMasterView;
	}

	public boolean isViewmobileMasterView() {
		return viewmobileMasterView;
	}

	public void setViewmobileMasterView(boolean viewmobileMasterView) {
		this.viewmobileMasterView = viewmobileMasterView;
	}

	public boolean isModifymobileMasterView() {
		return modifymobileMasterView;
	}

	public void setModifymobileMasterView(boolean modifymobileMasterView) {
		this.modifymobileMasterView = modifymobileMasterView;
	}

	public boolean isDeletemobileMasterView() {
		return deletemobileMasterView;
	}

	public void setDeletemobileMasterView(boolean deletemobileMasterView) {
		this.deletemobileMasterView = deletemobileMasterView;
	}

	public boolean isAddphoneDirectoryView() {
		return addphoneDirectoryView;
	}

	public void setAddphoneDirectoryView(boolean addphoneDirectoryView) {
		this.addphoneDirectoryView = addphoneDirectoryView;
	}

	public boolean isViewphoneDirectoryView() {
		return viewphoneDirectoryView;
	}

	public void setViewphoneDirectoryView(boolean viewphoneDirectoryView) {
		this.viewphoneDirectoryView = viewphoneDirectoryView;
	}

	public boolean isModifyphoneDirectoryView() {
		return modifyphoneDirectoryView;
	}

	public void setModifyphoneDirectoryView(boolean modifyphoneDirectoryView) {
		this.modifyphoneDirectoryView = modifyphoneDirectoryView;
	}

	public boolean isDeletephoneDirectoryView() {
		return deletephoneDirectoryView;
	}

	public void setDeletephoneDirectoryView(boolean deletephoneDirectoryView) {
		this.deletephoneDirectoryView = deletephoneDirectoryView;
	}

	public boolean isAddcontactGroupView() {
		return addcontactGroupView;
	}

	public void setAddcontactGroupView(boolean addcontactGroupView) {
		this.addcontactGroupView = addcontactGroupView;
	}

	public boolean isViewcontactGroupView() {
		return viewcontactGroupView;
	}

	public void setViewcontactGroupView(boolean viewcontactGroupView) {
		this.viewcontactGroupView = viewcontactGroupView;
	}

	public boolean isModifycontactGroupView() {
		return modifycontactGroupView;
	}

	public void setModifycontactGroupView(boolean modifycontactGroupView) {
		this.modifycontactGroupView = modifycontactGroupView;
	}

	public boolean isDeletecontactGroupView() {
		return deletecontactGroupView;
	}

	public void setDeletecontactGroupView(boolean deletecontactGroupView) {
		this.deletecontactGroupView = deletecontactGroupView;
	}

	public boolean isAddsmsDraftView() {
		return addsmsDraftView;
	}

	public void setAddsmsDraftView(boolean addsmsDraftView) {
		this.addsmsDraftView = addsmsDraftView;
	}

	public boolean isViewsmsDraftView() {
		return viewsmsDraftView;
	}

	public void setViewsmsDraftView(boolean viewsmsDraftView) {
		this.viewsmsDraftView = viewsmsDraftView;
	}

	public boolean isModifysmsDraftView() {
		return modifysmsDraftView;
	}

	public void setModifysmsDraftView(boolean modifysmsDraftView) {
		this.modifysmsDraftView = modifysmsDraftView;
	}

	public boolean isDeletesmsDraftView() {
		return deletesmsDraftView;
	}

	public void setDeletesmsDraftView(boolean deletesmsDraftView) {
		this.deletesmsDraftView = deletesmsDraftView;
	}

	public boolean isAddinstantMessageView() {
		return addinstantMessageView;
	}

	public void setAddinstantMessageView(boolean addinstantMessageView) {
		this.addinstantMessageView = addinstantMessageView;
	}

	public boolean isAddscheduleMessageView() {
		return addscheduleMessageView;
	}

	public void setAddscheduleMessageView(boolean addscheduleMessageView) {
		this.addscheduleMessageView = addscheduleMessageView;
	}

	public boolean isViewscheduleMessageView() {
		return viewscheduleMessageView;
	}

	public void setViewscheduleMessageView(boolean viewscheduleMessageView) {
		this.viewscheduleMessageView = viewscheduleMessageView;
	}

	public boolean isModifyscheduleMessageView() {
		return modifyscheduleMessageView;
	}

	public void setModifyscheduleMessageView(boolean modifyscheduleMessageView) {
		this.modifyscheduleMessageView = modifyscheduleMessageView;
	}

	public boolean isDeletescheduleMessageView() {
		return deletescheduleMessageView;
	}

	public void setDeletescheduleMessageView(boolean deletescheduleMessageView) {
		this.deletescheduleMessageView = deletescheduleMessageView;
	}

	public boolean isViewinstantMessageReportView() {
		return viewinstantMessageReportView;
	}

	public void setViewinstantMessageReportView(boolean viewinstantMessageReportView) {
		this.viewinstantMessageReportView = viewinstantMessageReportView;
	}

	public boolean isViewscheduleMessageReportView() {
		return viewscheduleMessageReportView;
	}

	public void setViewscheduleMessageReportView(boolean viewscheduleMessageReportView) {
		this.viewscheduleMessageReportView = viewscheduleMessageReportView;
	}

	@Override
	public String toString() {
		return "UserEntity [loginId=" + loginId + ", userName=" + userName + ", userType=" + userType + ", password="
				+ password + ", mobile=" + mobile + ", emailId=" + emailId + ", location=" + location + ", createdBy="
				+ createdBy + ", createDate=" + createDate + ", flag=" + flag + ", adduserMasterView="
				+ adduserMasterView + ", viewuserMasterView=" + viewuserMasterView + ", modifyuserMasterView="
				+ modifyuserMasterView + ", deleteuserMasterView=" + deleteuserMasterView + ", addmobileMasterView="
				+ addmobileMasterView + ", viewmobileMasterView=" + viewmobileMasterView + ", modifymobileMasterView="
				+ modifymobileMasterView + ", deletemobileMasterView=" + deletemobileMasterView
				+ ", addphoneDirectoryView=" + addphoneDirectoryView + ", viewphoneDirectoryView="
				+ viewphoneDirectoryView + ", modifyphoneDirectoryView=" + modifyphoneDirectoryView
				+ ", deletephoneDirectoryView=" + deletephoneDirectoryView + ", addcontactGroupView="
				+ addcontactGroupView + ", viewcontactGroupView=" + viewcontactGroupView + ", modifycontactGroupView="
				+ modifycontactGroupView + ", deletecontactGroupView=" + deletecontactGroupView + ", addsmsDraftView="
				+ addsmsDraftView + ", viewsmsDraftView=" + viewsmsDraftView + ", modifysmsDraftView="
				+ modifysmsDraftView + ", deletesmsDraftView=" + deletesmsDraftView + ", addinstantMessageView="
				+ addinstantMessageView + ", addscheduleMessageView=" + addscheduleMessageView
				+ ", viewscheduleMessageView=" + viewscheduleMessageView + ", modifyscheduleMessageView="
				+ modifyscheduleMessageView + ", deletescheduleMessageView=" + deletescheduleMessageView
				+ ", viewinstantMessageReportView=" + viewinstantMessageReportView + ", viewscheduleMessageReportView="
				+ viewscheduleMessageReportView + "]";
	}

	
	
}

	