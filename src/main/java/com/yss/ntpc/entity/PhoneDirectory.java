package com.yss.ntpc.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class PhoneDirectory {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long contactId;
	
	@Column(name="name")
	private String Name;
	
	@Column(name="mobile_No1")
	private String mobileNo1;
	
		
	@ManyToOne
	@JoinColumn(name="department_Id")
	private DepartmentEntity department;
	
	
	
	
	@Column(name="emp_Id")
	private String empId;
	
	
	@Column(name="email_Id")
	private String emailId;
		
	private String locationName;
	
	@Column(name="date_of_birth")
	private String dateofbirth;
	
	@Column(name="bdaySms")
	private String bdaySms;
	
	@Column(name="date_of_anniversary")
	private String dateofanniversary;
	
	@Column(name="anniversarySms")
	private String anniversarySms;
	
	@Column(name="create_Date")
	private String createDate;
	 
		
	@OneToOne
	@JoinColumn(name = "loginId")
	private UserEntity loginId;
	
	 @Column(name="flag", columnDefinition = "boolean default false")
		private boolean flag;
		
	
	public PhoneDirectory() {
		super();
		// TODO Auto-generated constructor stub
	}


	public PhoneDirectory(long contactId, String name, String mobileNo1, DepartmentEntity department, String empId,
			String emailId, String locationName, String dateofbirth, String bdaySms, String dateofanniversary,
			String anniversarySms, String createDate, UserEntity loginId, boolean flag) {
		super();
		this.contactId = contactId;
		Name = name;
		this.mobileNo1 = mobileNo1;
		this.department = department;
		this.empId = empId;
		this.emailId = emailId;
		this.locationName = locationName;
		this.dateofbirth = dateofbirth;
		this.bdaySms = bdaySms;
		this.dateofanniversary = dateofanniversary;
		this.anniversarySms = anniversarySms;
		this.createDate = createDate;
		this.loginId = loginId;
		this.flag = flag;
	}


	public long getContactId() {
		return contactId;
	}


	public void setContactId(long contactId) {
		this.contactId = contactId;
	}


	public String getName() {
		return Name;
	}


	public void setName(String name) {
		Name = name;
	}


	public String getMobileNo1() {
		return mobileNo1;
	}


	public void setMobileNo1(String mobileNo1) {
		this.mobileNo1 = mobileNo1;
	}


	public DepartmentEntity getDepartment() {
		return department;
	}


	public void setDepartment(DepartmentEntity department) {
		this.department = department;
	}


	public String getEmpId() {
		return empId;
	}


	public void setEmpId(String empId) {
		this.empId = empId;
	}


	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	public String getLocationName() {
		return locationName;
	}


	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}


	public String getDateofbirth() {
		return dateofbirth;
	}


	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}


	public String getBdaySms() {
		return bdaySms;
	}


	public void setBdaySms(String bdaySms) {
		this.bdaySms = bdaySms;
	}


	public String getDateofanniversary() {
		return dateofanniversary;
	}


	public void setDateofanniversary(String dateofanniversary) {
		this.dateofanniversary = dateofanniversary;
	}


	public String getAnniversarySms() {
		return anniversarySms;
	}


	public void setAnniversarySms(String anniversarySms) {
		this.anniversarySms = anniversarySms;
	}


	public String getCreateDate() {
		return createDate;
	}


	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}


	public UserEntity getLoginId() {
		return loginId;
	}


	public void setLoginId(UserEntity loginId) {
		this.loginId = loginId;
	}


	public boolean isFlag() {
		return flag;
	}


	public void setFlag(boolean flag) {
		this.flag = flag;
	}


	@Override
	public String toString() {
		return "PhoneDirectory [contactId=" + contactId + ", Name=" + Name + ", mobileNo1=" + mobileNo1
				+ ", department=" + department + ", empId=" + empId + ", emailId=" + emailId + ", locationName="
				+ locationName + ", dateofbirth=" + dateofbirth + ", bdaySms=" + bdaySms + ", dateofanniversary="
				+ dateofanniversary + ", anniversarySms=" + anniversarySms + ", createDate=" + createDate + ", loginId="
				+ loginId + ", flag=" + flag + "]";
	}


	
	
}
