package com.yss.ntpc.entity;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "tbl_outsmshistory")
public class ReportEntity  implements Serializable{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = -6589342057704836094L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="Fld_RowID")
	private long id;

//	@Column(name = "altid" ,  nullable = true, columnDefinition = "bigint(20) default 0")
//	private long altid;
//	
//	@Column(name = "armyid" , nullable = true, columnDefinition = "varchar(255) default ''")
//	private String armyid;
//	
//	
//	@Column(name = "name" , nullable = true, columnDefinition = "varchar(255) default ''")
//	private String name;
//
//	@Column(name = "rank" , nullable = true, columnDefinition = "varchar(255) default ''")
//	private String rank;
//
//	@Column(name = "group_name" , nullable = true, columnDefinition = "varchar(255) default ''")
//	private String group_name;
//
//
//	
//	@Column(name = "reason" , nullable = true, columnDefinition = "varchar(255) default ''")
//	private String reason;
//	
//	@Column(name = "response" , nullable = true, columnDefinition = "varchar(255) default ''")
//	private String response;

	@Column(name="Fld_SMSID")
	private String Fld_SMSID;
	

//	@Column(name = "instanceMessageReport", columnDefinition = "boolean default false")
//	private boolean instanceMessageReportView;
//
//	@Column(name = "instanceVoiceReport", columnDefinition = "boolean default false")
//	private boolean instanceVoiceReportView;
//	
//	@Column(name = "scheduleVoiceReport", columnDefinition = "boolean default false")
//	private boolean scheduleVoiceReportView;
//	
//	
//	@Column(name = "scheduleMessageReport", columnDefinition = "boolean default false")
//	private boolean scheduleMessageReportView;
//	
//	@Column(name = "smsTrunkReport", columnDefinition = "boolean default false")
//	private boolean smsTrunkReportView;
	

	@Column(name = "Fld_SMSText", nullable = true, columnDefinition = "varchar(255) default ''")
	private String smsText;


//	@Column(name = "status" , nullable = true, columnDefinition = "varchar(255) default ''")
//	private String status;
	
	@Column(name = "Fld_SentDateTime", nullable = true, columnDefinition = "varchar(255) default ''")
	private String scheduleDate;
	
	
	@Column(name = "Fld_TrunkNumber")
	private String Fld_TrunkNumber;

//	@Column(name = "contactGroupName")
//	private String contactGroupName;

//	@Column(name = "departmentName")
//	private String departmentName;


	@Column(name = "Fld_CLI", nullable = true, columnDefinition = "varchar(255) default 0")
	private String mobile;

//	@Column(name = "scheduleType")
//	private String scheduleType;



//	@Column(name = "sendBy")
//	private String sendBy;

	@Column(name = "Fld_IsSent", columnDefinition = "boolean default false", nullable = true)
	private boolean flag;

//	@Column(name = "priorty", nullable = true, columnDefinition = "varchar(255) default 0")
//	private String priority;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "insertdatetime" , nullable = false, columnDefinition = "timestamp default CURRENT_TIMESTAMP")
	private Date insertdatetime;
	

	public ReportEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getFld_SMSID() {
		return Fld_SMSID;
	}


	public void setFld_SMSID(String fld_SMSID) {
		Fld_SMSID = fld_SMSID;
	}


	public String getSmsText() {
		return smsText;
	}


	public void setSmsText(String smsText) {
		this.smsText = smsText;
	}


	public String getScheduleDate() {
		return scheduleDate;
	}


	public void setScheduleDate(String scheduleDate) {
		this.scheduleDate = scheduleDate;
	}


	public String getFld_TrunkNumber() {
		return Fld_TrunkNumber;
	}


	public void setFld_TrunkNumber(String fld_TrunkNumber) {
		Fld_TrunkNumber = fld_TrunkNumber;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public boolean isFlag() {
		return flag;
	}


	public void setFlag(boolean flag) {
		this.flag = flag;
	}


	public Date getInsertdatetime() {
		return insertdatetime;
	}


	public void setInsertdatetime(Date insertdatetime) {
		this.insertdatetime = insertdatetime;
	}


	@Override
	public String toString() {
		return "ReportEntity [id=" + id + ", Fld_SMSID=" + Fld_SMSID + ", smsText=" + smsText + ", scheduleDate="
				+ scheduleDate + ", Fld_TrunkNumber=" + Fld_TrunkNumber + ", mobile=" + mobile + ", flag=" + flag
				+ ", insertdatetime=" + insertdatetime + "]";
	}

	
	

	
	
}
