package com.yss.ntpc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_outsms")
public class InstanceMessageDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Fld_SMSID")
	private long id;

	@Column(name = "Fld_TrunkNumber")
	private String Fld_TrunkNumber;

	
	// @Column(name = "altid" , nullable = true, columnDefinition = "bigint(20)
	// default 0")
	// private long altid;
	//
	// @Column(name = "armyid" , nullable = true, columnDefinition = "varchar(255)
	// default ''")
	// private String armyid;
	//
	//
	// @Column(name = "name" , nullable = true, columnDefinition = "varchar(255)
	// default ''")
	// private String name;
	//
	// @Column(name = "rank" , nullable = true, columnDefinition = "varchar(255)
	// default ''")
	// private String rank;
	//
	// @Column(name = "group_name" , nullable = true, columnDefinition =
	// "varchar(255) default ''")
	// private String group_name;
	//
	// @Column(name = "status" , nullable = true, columnDefinition = "varchar(255)
	// default ''")
	// private String status;

	// @Column(name="phoneDirectoryName")
	// private String phoneDirectoryName;
	//
	// @Column(name="contactGroupName")
	// private String contactGroupName;
	//
	// @Column(name="departmentName")
	// private String departmentName;

	// @Column(name="smsName")
	// private String smsName;

	@Column(name = "Fld_SMSText", nullable = true, columnDefinition = "varchar(255) default ''")
	private String smsText;

	// private String smsVoice;

	@Column(name = "Fld_MobileNo", nullable = true, columnDefinition = "varchar(255) default 0")
	private String mobile;

	// @Column(name="scheduleType")
	// private String scheduleType;

	// @Column(name="scheduled" , nullable = true, columnDefinition = "varchar(255)
	// default ''")
	// private String scheduleDate;

	@Column(name = "Fld_SentDateTime")
	private String dateAndTime;

	// @Column(name="sendBy")
	// private String sendBy;

	@Column(name = "Fld_isSent", columnDefinition = "boolean default false", nullable = true)
	private boolean flag;

	// @Column(name="priorty" , nullable = true, columnDefinition = "varchar(255)
	// default 0")
	// private String priority;

	public InstanceMessageDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InstanceMessageDetails(long id, String fld_TrunkNumber, String smsText, String mobile, String dateAndTime,
			boolean flag) {
		super();
		this.id = id;
		Fld_TrunkNumber = fld_TrunkNumber;
		this.smsText = smsText;
		this.mobile = mobile;
		this.dateAndTime = dateAndTime;
		this.flag = flag;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFld_TrunkNumber() {
		return Fld_TrunkNumber;
	}

	public void setFld_TrunkNumber(String fld_TrunkNumber) {
		Fld_TrunkNumber = fld_TrunkNumber;
	}

	public String getSmsText() {
		return smsText;
	}

	public void setSmsText(String smsText) {
		this.smsText = smsText;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "InstanceMessageDetails [id=" + id + ", Fld_TrunkNumber=" + Fld_TrunkNumber + ", smsText=" + smsText
				+ ", mobile=" + mobile + ", dateAndTime=" + dateAndTime + ", flag=" + flag + "]";
	}

}
