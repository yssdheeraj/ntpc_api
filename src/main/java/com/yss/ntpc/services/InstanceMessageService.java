package com.yss.ntpc.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yss.ntpc.entity.ContactGroup;
import com.yss.ntpc.entity.DepartmentEntity;
import com.yss.ntpc.entity.GroupWithContact;
import com.yss.ntpc.entity.InstanceMessageDetails;
import com.yss.ntpc.entity.InstanceMsgEntity;
import com.yss.ntpc.entity.PhoneDirectory;
import com.yss.ntpc.entity.ReportEntity;
import com.yss.ntpc.entity.SMSEntity;
import com.yss.ntpc.entity.ScheduleMessageEntity;
import com.yss.ntpc.generic.service.IGenericService;


@Service
@Transactional
public class InstanceMessageService {
	
	private static final Logger logger = LoggerFactory.getLogger(InstanceMessageService.class);
	@Autowired
	private IGenericService<DepartmentEntity> departmentDataGenericService;
	
	@Autowired
	private IGenericService<GroupWithContact> allContactGroupGenericService;
	
	@Autowired
	private IGenericService<ReportEntity> reportEntityService;
	
	@Autowired
	private IGenericService<ContactGroup> contactGroupService;
	
	@Autowired
	private IGenericService<PhoneDirectory> phoneDirectoryService;
	
	@Autowired
	private IGenericService<SMSEntity> ismsService;

	@Autowired
	private IGenericService<InstanceMessageDetails> instanceMessageDetailsService;
	
	@Autowired
	private IGenericService<ScheduleMessageEntity> scheduleMessageEntityService; 
	
	private String MESSAGE="";
	private String SCHEDULE_DATE;
	private String PRIORITY="";
	private String SCHEDULE_TYPE=""; 
	private String SMS_NAME="";
	private String GROUP_NAME="";
	private String USER_ID="";
	Date TODAY_DATE = new Date();
	HashSet<String> filter=new HashSet<String>();
	List<PhoneDirectory> duplicateList=new ArrayList<PhoneDirectory>();
	
	
	
	/******************************** FOR SCHEDULE MESSAGE **************************************************************/
	public void saveScheduledMessage(InstanceMsgEntity instanceMsgEntity) {
		
		filter.clear();
		SCHEDULE_DATE=instanceMsgEntity.getScheduleDate();
		PRIORITY = instanceMsgEntity.getPriority();
		SCHEDULE_TYPE = instanceMsgEntity.getScheduleType();
		SMS_NAME = instanceMsgEntity.getSmsName();
		USER_ID = instanceMsgEntity.getLoginId().getLoginId();
		
		if(instanceMsgEntity.getSmsName() !=null ||instanceMsgEntity.getSmsText() !=null ) {
			MESSAGE=getRealMessage(instanceMsgEntity.getSmsName(),instanceMsgEntity.getSmsText());
			logger.info("message created..... group..............................");
		
			if (instanceMsgEntity.getPhoneDirectoryName() !=null) {
				getPhoneDirectoryByMobileList(instanceMsgEntity.getPhoneDirectoryName());
				logger.info("----Phonedirectory List Fetched..............................");
			}
		
			if (instanceMsgEntity.getContactGroupName() !=null ) {
				getPhoneDirectoryListByGroups(instanceMsgEntity.getContactGroupName());
				logger.info("-----Contact Group List Fetched..............................");
			}
			
			if(instanceMsgEntity.getDepartmentName()!=null ) {
				getPhoneDirectoryListByDepartmentNameList(instanceMsgEntity.getDepartmentName());
				logger.info("-----Department List Fetched ..............................");
			}
			
			
			if(instanceMsgEntity.getMobile() !=null ) {
				getPhoneDirectoryByMobileNumber(instanceMsgEntity.getMobile());
				logger.info("------Enter Mobile Number Fetched..............................");
			}
		}
		
		else {
			logger.info("----Please Enter Sms-----");
		}
		
		
	}
	
	public void saveInstaceMessage(InstanceMsgEntity instanceMsgEntity) {
		
//			logger.info("directory"+instanceMsgEntity.getPhoneDirectoryName());
//			logger.info("directory"+instanceMsgEntity.getContactGroupName());
//			logger.info("directory"+instanceMsgEntity.getDepartMentName());
//			logger.info("directory"+instanceMsgEntity.getMobile());
//			logger.info("directory"+instanceMsgEntity.getSenderBy());
//			logger.info("directory"+instanceMsgEntity.getSmsName());
//			logger.info("directory"+instanceMsgEntity.getSmsVoice());
//			logger.info("directory"+instanceMsgEntity.getSmstxt());
//			logger.info("directory"+instanceMsgEntity.getDateANDTime());
//			logger.info("directory"+instanceMsgEntity.isFlage());
		
//			logger.info("directory"+instanceMsgEntity.getPhoneDirectoryName());
//			logger.info("directory"+instanceMsgEntity.getContactGroupName());
//			logger.info("directory"+instanceMsgEntity.getDepartMentName());
//			logger.info("directory"+instanceMsgEntity.getMobile());
//			logger.info("directory"+instanceMsgEntity.getSenderBy());
//			logger.info("directory"+instanceMsgEntity.getSmsName());
//			logger.info("directory"+instanceMsgEntity.getSmsVoice());
//			logger.info("directory"+instanceMsgEntity.getSmstxt());
//			logger.info("directory"+instanceMsgEntity.getDateANDTime());
//			logger.info("directory"+instanceMsgEntity.isFlage());
//			
		//get Sms
			SCHEDULE_DATE=instanceMsgEntity.getScheduleDate();
			SCHEDULE_TYPE=instanceMsgEntity.getScheduleType();
			PRIORITY=instanceMsgEntity.getPriority();
			SMS_NAME = instanceMsgEntity.getSmsName();

		if(instanceMsgEntity.getSmsName()!=null ||instanceMsgEntity.getSmsText() !=null ) {
			MESSAGE=getRealMessage(instanceMsgEntity.getSmsName(),instanceMsgEntity.getSmsText());
			logger.info("message created..... group..............................");
			
			//save data according phone directory
			if(instanceMsgEntity.getPhoneDirectoryName() !=null ) {
				getPhoneDirectoryByMobileList(instanceMsgEntity.getPhoneDirectoryName());
				logger.info("data save using... phonedirectory list..............................");
			}
			
			//save data according to group
			
			if(instanceMsgEntity.getContactGroupName() !=null )
			{
				getPhoneDirectoryListByGroups(instanceMsgEntity.getContactGroupName());
				logger.info("data save using... group..............................");
			}
			
			//save data according to department
			if(instanceMsgEntity.getDepartmentName()!=null) {
				getPhoneDirectoryListByDepartmentNameList(instanceMsgEntity.getDepartmentName());
				logger.info("data save using... department ..............................");
			}
			
			//save data according phone number
			if(instanceMsgEntity.getMobile() !=null ) {
				getPhoneDirectoryByMobileNumber(instanceMsgEntity.getMobile());
				logger.info("data save using... mobile number..............................");
			}
		}
			
	}
	
	

	//create real sms 
	public String  getRealMessage(String smsName,String smstxt) {
		String message="";
		if(smsName !=null && !smsName.equals("")) {
			SMSEntity sMSEntity=ismsService.find(new SMSEntity(),"where smsName = '"+smsName+"'");
			message=sMSEntity.getSmsDesc()+" \n "+smstxt;
		}
		else
		{
			message=smstxt;
		}
		logger.info("This is real sms created  "+message);	
		return message;
		}
	
	
	//get phone directory by mobile number list
	public void getPhoneDirectoryByMobileList(ArrayList<String> phoneDirectoryName){
		if(phoneDirectoryName !=null)
		{
			for(String mobile1 :phoneDirectoryName ) {
				
				
				ScheduleMessageEntity  scheduleMessageEntity = new ScheduleMessageEntity(); 
				//InstanceMessageDetails instanceMessageDetails=new InstanceMessageDetails();
				PhoneDirectory phoneDirectory=phoneDirectoryService.find(new PhoneDirectory(), "where mobileNo1 = '"+mobile1+"'");
				if(phoneDirectory !=null) {
				
					
						savedataInInstanceSMSDB(phoneDirectory);
				
					
				}
				
								
			}
		}
		
	}
	
	
	//get phone directory by mobile number 
	public void getPhoneDirectoryByMobileNumber(String mobile1) {
		
		/*InstanceMessageDetails instanceMessageDetails=new InstanceMessageDetails();
		instanceMessageDetails.setMobile(mobile1);
		instanceMessageDetails.setDateAndTime(new Date().toString());
		instanceMessageDetails.setSendBy("Admin");
		instanceMessageDetails.setSmsText(MESSAGE);
		instanceMessageDetails.setPriority(PRIORITY);
		instanceMessageDetails.setScheduleDate(SCHEDULE_DATE);
		instanceMessageDetails.setScheduleType(SCHEDULE_TYPE);
		instanceMessageDetails.setFlag(true);*/
		
		ScheduleMessageEntity  scheduleMessageEntity = new ScheduleMessageEntity(); 
		
		scheduleMessageEntity.setMobile(mobile1);
		scheduleMessageEntity.setDateTime(new Date().toString());
		scheduleMessageEntity.setSendBy(USER_ID);
		scheduleMessageEntity.setSmsText(MESSAGE);
		scheduleMessageEntity.setScheduleDate(SCHEDULE_DATE);
		scheduleMessageEntity.setPhoneDirectoryName("Unknown");
		scheduleMessageEntity.setScheduleType(SCHEDULE_TYPE);
		scheduleMessageEntity.setPriority(PRIORITY);
		scheduleMessageEntity.setFlag(true);
		scheduleMessageEntity.setSmsName(SMS_NAME);
		logger.info("data save according ot phone directory----------------------------"+scheduleMessageEntity);
		scheduleMessageEntityService.save(scheduleMessageEntity);
	
	
		
	}
	
	//get phone directory list by contact group
	public void getPhoneDirectoryListByGroups(ArrayList<String> contactGroupName) {
		
		if(contactGroupName !=null)
		{
			GROUP_NAME="";
			for(String groupName :contactGroupName ) {
				GROUP_NAME = groupName;
				ContactGroup contactGroup=contactGroupService.find(new ContactGroup(), "where contactGroupName = '"+groupName+"'");
				
				List<GroupWithContact> contactList = allContactGroupGenericService.findAllByCondition(new GroupWithContact(), "where contactGroup='"+contactGroup.getContact_groupId()+"'");
				System.out.println("####################################" +contactList);
				logger.info("data save according ot phone directory----------------------------"+contactGroup);
				logger.info("constact list "+contactList);
				
				

				for (GroupWithContact groupWithContact : contactList) {
					
					PhoneDirectory phoneDirectory=groupWithContact.getPhoneDirectory();
					
					if(phoneDirectory !=null) {
							savedataInInstanceSMSDB(phoneDirectory);
					}
					
				}
				

			}
		}
		
	}
	
	
	//get phone directory list by department list
	public void getPhoneDirectoryListByDepartmentNameList(ArrayList<String> departmentNameList)
	{
		if(departmentNameList!=null) {
			for(String department:departmentNameList) {
				DepartmentEntity departmentData=departmentDataGenericService.find(new DepartmentEntity(),"where departmentName = '"+department+"'");
				logger.info("department "+departmentData.toString());
			List<PhoneDirectory> phoneDirectoryList=phoneDirectoryService.fetch(new PhoneDirectory(),"where department = '"+departmentData.getDepartmentId()+"'");
				for(PhoneDirectory phoneDirectory:phoneDirectoryList) {
					
					if(phoneDirectory !=null) {
							
							savedataInInstanceSMSDB(phoneDirectory);
					
					}
					}
			}
		}
		
		
	}
	
	
	//save data using phone directory
	public void savedataInInstanceSMSDB(PhoneDirectory phoneDirectory){
		//InstanceMessageDetails instanceMessageDetails=new InstanceMessageDetails();
		ScheduleMessageEntity  scheduleMessageEntity = new ScheduleMessageEntity(); 
		//ReportEntity reportEntity = new ReportEntity(); 
		if(phoneDirectory !=null) {
			
//			instanceMessageDetails.setSmsName(phoneDirectory.getName());
//			instanceMessageDetails.setMobile(phoneDirectory.getMobileNo1());
//			instanceMessageDetails.setDateAndTime(new Date().toString());
//			instanceMessageDetails.setSendBy("Admin");
//			instanceMessageDetails.setSmsText(MESSAGE);
//			instanceMessageDetails.setPriority(PRIORITY);
//			instanceMessageDetails.setScheduleDate(SCHEDULE_DATE);
//			instanceMessageDetails.setScheduleType(SCHEDULE_TYPE);
//			instanceMessageDetails.setFlag(true);
			
			System.out.println(phoneDirectory);
			
			boolean result=filter.add(phoneDirectory.getMobileNo1());
			if(result) {
				//========================================================================================================			
				//scheduleMessageEntity.setDepartmentName(phoneDirectory.getDepartment().getDepartmentName());
				scheduleMessageEntity.setContactGroupName(GROUP_NAME);
				scheduleMessageEntity.setSmsName(SMS_NAME);
				scheduleMessageEntity.setPhoneDirectoryName(phoneDirectory.getName());
				scheduleMessageEntity.setMobile(phoneDirectory.getMobileNo1());
				scheduleMessageEntity.setDateTime(new Date().toString());
				scheduleMessageEntity.setSendBy(USER_ID);
				scheduleMessageEntity.setSmsText(MESSAGE);
				scheduleMessageEntity.setPriority(PRIORITY);
				scheduleMessageEntity.setScheduleDate(SCHEDULE_DATE);
				scheduleMessageEntity.setScheduleType(SCHEDULE_TYPE);
				scheduleMessageEntity.setFlag(true);
				
				logger.info("data save according ot phone directory----------------------------"+scheduleMessageEntity);
				scheduleMessageEntityService.save(scheduleMessageEntity);
				
				
	//==================================================================================================
				
			}else {
				duplicateList.add(phoneDirectory);
				logger.info("**********************************************************");
				logger.info("dupicate value   "+phoneDirectory);
				logger.info("**********************************************************");
			}
			
			
//========================================================================================================			
			/*scheduleMessageEntity.setDepartmentName(phoneDirectory.getDepartment().getDepartmentName());
			scheduleMessageEntity.setContactGroupName(GROUP_NAME);
			scheduleMessageEntity.setSmsName(SMS_NAME);
			scheduleMessageEntity.setPhoneDirectoryName(phoneDirectory.getName());
			scheduleMessageEntity.setMobile(phoneDirectory.getMobileNo1());
			scheduleMessageEntity.setDateTime(new Date().toString());
			scheduleMessageEntity.setSendBy(USER_ID);
			scheduleMessageEntity.setSmsText(MESSAGE);
			scheduleMessageEntity.setPriority(PRIORITY);
			scheduleMessageEntity.setScheduleDate(SCHEDULE_DATE);
			scheduleMessageEntity.setScheduleType(SCHEDULE_TYPE);
			scheduleMessageEntity.setFlag(true);
			
			logger.info("data save according ot phone directory----------------------------"+scheduleMessageEntity);
			scheduleMessageEntityService.save(scheduleMessageEntity);
			*/
			
//==================================================================================================
			
//			reportEntity.setContactGroupName(instanceMessageDetails.getContactGroupName());
//			reportEntity.setMobile(instanceMessageDetails.getMobile());
//			reportEntity.setDateAndTime(instanceMessageDetails.getDateAndTime());
//			reportEntity.setSendBy(instanceMessageDetails.getSendBy());
//			reportEntity.setSmsText(instanceMessageDetails.getSmsText());
//			reportEntity.setPriority(instanceMessageDetails.getPriority());
//			reportEntity.setScheduleDate(instanceMessageDetails.getScheduleDate());
//			reportEntity.setScheduleType(instanceMessageDetails.getScheduleType());
			
			//logger.info("data save according ot phone directory in ReportEntity----------------------------"+reportEntity);
			//reportEntityService.save(reportEntity);
			
	
		}
	}
		
	public List<PhoneDirectory> getDuplicatePhoneDirectory(){
		
		logger.info("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
		return duplicateList;
	}
	
	
	//fetch a smslist
	

}
