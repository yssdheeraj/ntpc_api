package com.yss.ntpc.generic.service;

import java.io.Serializable;
import java.util.List;

public interface IGenericService<T> {

	void save(T entity);

	void update(T entity);

	void delete(T entity);

	List<T> fetch(T entity);

	List<T> fetch(T entity, String condition);

	T find(T entity, Serializable id);

	T find(T entity, String condition);

	boolean exists(T entity, Serializable id);

	boolean exists(T entity, String condition);

	List<T> nativeQuery(T entity, String query);
 
	List<T> findAllByCondition(T entity , String condition);
}
