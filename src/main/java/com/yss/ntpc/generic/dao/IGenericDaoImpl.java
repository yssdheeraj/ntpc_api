package com.yss.ntpc.generic.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class IGenericDaoImpl<T> implements IGenericDao<T> {

	
	@PersistenceContext
	private EntityManager entityManager; 
	
	/*@Autowired
	   SessionFactory sessionFactory;; 
*/
		@Override
	public void save(T entity) {
		// TODO Auto-generated method stub
			entityManager.persist(entity);
	}

	@Override
	public void update(T entity) {
		// TODO Auto-generated method stub
		
		entityManager.merge(entity);
		
	}

	@Override
	public void delete(T entity) {
		// TODO Auto-generated method stub
		entityManager.remove(entity);
	}

	@Override
	public List<T> fetch(T entity) {
		// TODO Auto-generated method stub
		return entityManager.createQuery("from " + entity.getClass().getName()).getResultList();
		
	}

	@Override
	public List<T> fetch(T entity, String condition) {
		return entityManager.createQuery("from " + entity.getClass().getName() + " " + condition).getResultList();
		
	}

	@Override
	public T find(T entity, Serializable id) {
		// TODO Auto-generated method stub
		return (T) entityManager.find(entity.getClass(), id);
		
	}

	@Override
	public T find(T entity, String condition) {
		try {
			
			return (T) entityManager.createQuery("from " + entity.getClass().getName()+" " + condition).getResultList().get(0);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}
	

	
	

	@Override
	public List<T> nativeQuery(T entity, String query) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean exists(T entity, Serializable id) {		
		return find(entity, id) != null ? true : false;
	}

	@Override
	public boolean exists(T entity, String condition) {		
		return find(entity, condition) != null ? true : false;
	}

	@Override
	public List<T> findAllByCondition(T entity, String condition) {
		
		if (condition != null && condition != "") {
			try {
				System.out.println("----Fetch By All in If---- ");
				return entityManager.createQuery(
						"from " + entity.getClass().getName()+" obj "+ condition).getResultList();
			} catch (Exception e) {
				System.out.println("----Error In Fetch By All ---- " +e.getMessage());
				return null;
			}
		} else {
			System.out.println("----Fetch By All in else ---- ");
			return entityManager.createQuery(
					"from " + entity.getClass().getName()).getResultList();
		}
	}


}
