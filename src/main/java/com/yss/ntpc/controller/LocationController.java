package com.yss.ntpc.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.yss.ntpc.entity.LocationEntity;
import com.yss.ntpc.generic.service.IGenericService;

@RestController
@CrossOrigin
public class LocationController {

	private static final Logger logger = LoggerFactory.getLogger(LocationController.class);

	@Autowired
	private IGenericService<LocationEntity> iDataGenericService;

	@RequestMapping("/locationlist")
	public ResponseEntity<List<LocationEntity>> getLocationData() {

		List<LocationEntity> locationList = iDataGenericService.fetch(new LocationEntity(),"WHERE flag  = false");
		return new ResponseEntity<List<LocationEntity>>(locationList, HttpStatus.OK);
	}

	@PostMapping("/createlocation")
	public ResponseEntity<?> saveLocation(@RequestBody LocationEntity objLocation) {

		Date date = new Date();
		logger.info(date.toGMTString());
		logger.info("Data update successfully !" + objLocation);
		objLocation.setCreateDate(date.toGMTString());

		iDataGenericService.save(objLocation);

		return new ResponseEntity<>(iDataGenericService.fetch(objLocation), HttpStatus.CREATED);
	}

	@PutMapping("/updatelocation")
	public ResponseEntity<?> updateLocation(@RequestBody LocationEntity objLocation) {
		iDataGenericService.update(objLocation);
		logger.info("Data update successfully !" + objLocation);
		return new ResponseEntity<>(iDataGenericService.fetch(objLocation),HttpStatus.CREATED);
	}

	@RequestMapping(value = "/deletelocation/{locationId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteLocation(@PathVariable("locationId") long locationId) {
		LocationEntity objLocation = iDataGenericService.find(new LocationEntity(),locationId);
		if (objLocation == null) {
			return new ResponseEntity<>(objLocation, HttpStatus.NO_CONTENT);
		} else {
			objLocation.setFlag(true);
			iDataGenericService.update(objLocation);
			logger.info("Data deleted successfully !");
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}

	@PostMapping("/findlocation/id")
	public ResponseEntity<?> findByLocationId(@RequestBody LocationEntity objLocation) {

		LocationEntity location = iDataGenericService.find(objLocation, objLocation.getLocationId());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(location, HttpStatus.CREATED);
	}

	@PostMapping("/findlocation/name")
	public ResponseEntity<?> findByLocationame(@RequestBody LocationEntity objLocation) {

		LocationEntity location = iDataGenericService.find(objLocation, objLocation.getLocationName());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(location, HttpStatus.CREATED);
	}

}
