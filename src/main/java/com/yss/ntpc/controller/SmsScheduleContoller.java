package com.yss.ntpc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yss.ntpc.entity.InstanceMessageDetails;
import com.yss.ntpc.entity.SMSEntity;
import com.yss.ntpc.entity.ScheduleMessageEntity;
import com.yss.ntpc.generic.service.IGenericService;

@RestController
@CrossOrigin
public class SmsScheduleContoller {

	
	private static final Logger logger = LoggerFactory.getLogger(SmsController.class);
	
	@Autowired
	private IGenericService<ScheduleMessageEntity> scheduleMessageEntityService;
	
	@RequestMapping("/smsschedulelist")
	public  ResponseEntity<List<ScheduleMessageEntity>> getSMSData() {

		logger.info("woriking");
		 List<ScheduleMessageEntity> scheduleMessage=scheduleMessageEntityService.fetch(new ScheduleMessageEntity());
		return new ResponseEntity<List<ScheduleMessageEntity>>(scheduleMessage,HttpStatus.OK);
	}
	

	
	@PutMapping("/updatesmsschedule")
	public ResponseEntity<?> updateSMS(@RequestBody ScheduleMessageEntity objTextSMS) {
		scheduleMessageEntityService.update(objTextSMS);
		logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}


	@RequestMapping(value = "/deletesmsscheduless/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteSMS(@PathVariable("id") long id) {
		
		logger.info("TextSMS id "+id);
		ScheduleMessageEntity objSMS = scheduleMessageEntityService.find(new ScheduleMessageEntity(),id);
		if (objSMS == null) {
			logger.info("no data found !");
			return new ResponseEntity<>(objSMS, HttpStatus.NO_CONTENT);
		}
		else
		{
			objSMS.setFlag(true);
			scheduleMessageEntityService.delete(objSMS);
			logger.info("Data deleted successfully !");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		
	}

	
}
