package com.yss.ntpc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.ntpc.entity.AllContactGroup;
import com.yss.ntpc.entity.ContactGroup;
import com.yss.ntpc.entity.DepartmentEntity;
import com.yss.ntpc.entity.GroupWithContact;
import com.yss.ntpc.entity.PhoneDirectory;
import com.yss.ntpc.generic.service.IGenericService;

@RestController
@CrossOrigin
public class AllContactGroupController {

	
private static final Logger logger = LoggerFactory.getLogger(AllContactGroupController.class);

@Autowired
private IGenericService<GroupWithContact> iDataGenericService;

@Autowired
private IGenericService<PhoneDirectory> iDataPhoneDirectoryGenericService;


@PostMapping("/addContactGrp")
public  ResponseEntity<?> saveGroup(@RequestBody AllContactGroup objAllContactGroup) {
	logger.info("Data saved successfully,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,!");
	

	logger.info("group"+objAllContactGroup.getContactGroupName());
	logger.info("group"+objAllContactGroup.getPhoneDirectoryId());
	logger.info("group"+objAllContactGroup);	
	
	for(PhoneDirectory phone:objAllContactGroup.getPhoneDirectoryId())
	{
		GroupWithContact group=new GroupWithContact();
		group.setContactGroup(objAllContactGroup.getContactGroupName());
		group.setLoginId(objAllContactGroup.getLoginId());
		group.setCreateDate(new Date().toString());
		group.setPhoneDirectory(phone);
		iDataGenericService.update(group);
	}
		logger.info("Data saved successfully !"+objAllContactGroup.toString());
return new ResponseEntity(HttpStatus.OK);
}


@RequestMapping("/allgrouplist")
public  ResponseEntity<List<GroupWithContact>> getAllContactGroupData() {

	logger.info("Data Retrive successfully");
	 List<GroupWithContact> allgroupList=iDataGenericService.fetch(new GroupWithContact());
	return new ResponseEntity<List<GroupWithContact>>(allgroupList,HttpStatus.OK);
}


//@PostMapping("/findAllContactGroup/name")
//public ResponseEntity<?> findByAllContactGroupName(@RequestBody AllContactGroup objAllContactGroup) {
//	
//	AllContactGroup AllContactGroup=iDataGenericService.find(objAllContactGroup,"where contactGroupName = '" +objAllContactGroup.getContactGroupName()+"'");
//	logger.info("Data saved successfully !");
//	return new ResponseEntity<>(AllContactGroup,HttpStatus.CREATED);
//}

@RequestMapping(value="/removecontactfromgroup", method=RequestMethod.POST)
public  ResponseEntity<?> removeContactFromGroup(@RequestParam(value="phoneDirectoryId") long phoneDirectoryId,
										@RequestParam(value="groupId") long groupId 
											){
	int result=0;
	logger.info(phoneDirectoryId+"Data Retrive successfully   "+groupId);
	
		List<GroupWithContact> contactList = iDataGenericService.fetch(new GroupWithContact());
		for(GroupWithContact contact:contactList) {
			if((contact.getContactGroup().getContact_groupId()==groupId) && (contact.getPhoneDirectory().getContactId()==phoneDirectoryId)) {
				iDataGenericService.delete(contact);
				result=1;
				logger.info("data successfully deleted");
				break;
			}
			else {
				result=0;
			}
		}
		if(result==1) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		 
}

@RequestMapping("/ongroupcontact/{id}")
public  ResponseEntity<List<PhoneDirectory>> oneGroupContact(@PathVariable ("id") long id) {
	logger.info("one group data ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,! "+id );
	 List<GroupWithContact> onegroupList=new ArrayList<GroupWithContact>();
	 GroupWithContact contact1=new GroupWithContact();
	 //for get group phone directory list 
	 List<PhoneDirectory> phoneDirectoryList=new ArrayList<PhoneDirectory>();
	 List<GroupWithContact> allgroupList=iDataGenericService.fetch(new GroupWithContact()); 
	for(GroupWithContact contact:allgroupList)
	{
		if(contact.getContactGroup().getContact_groupId()==id)
		{
			onegroupList.add(contact);
			logger.info("get direcrory"+contact.getPhoneDirectory());
			phoneDirectoryList.add(contact.getPhoneDirectory());
		}
	}
		logger.info("Data get successfully !"+phoneDirectoryList.size());
return new ResponseEntity<List<PhoneDirectory>>(phoneDirectoryList,HttpStatus.OK);
}

@RequestMapping("/showallcontact/{id}")
public  ResponseEntity<List<PhoneDirectory>> showAllContactNotInGroup(@PathVariable ("id") long id) {
		int temp=0;
	logger.info("one group data ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,! "+id );
	 List<GroupWithContact> onegroupList=new ArrayList<GroupWithContact>();
	 GroupWithContact contact1=new GroupWithContact();
	 //for get group phone directory list 
	 List<PhoneDirectory> groupPhoneDirectoryList=new ArrayList<PhoneDirectory>();
	 List<PhoneDirectory> notInGroupPhoneDirectoryList=new ArrayList<PhoneDirectory>();
	 List<GroupWithContact> allgroupList=iDataGenericService.fetch(new GroupWithContact()); 
	for(GroupWithContact contact:allgroupList)
	{
		if(contact.getContactGroup().getContact_groupId()==id)
		{
			onegroupList.add(contact);
			logger.info("get add direcrory"+contact.getPhoneDirectory());
			groupPhoneDirectoryList.add(contact.getPhoneDirectory());
		}
	}
	
	List<PhoneDirectory> allPhoneDirectoryList1=iDataPhoneDirectoryGenericService.fetch(new PhoneDirectory(),"where flag = false");
	for(PhoneDirectory phone:allPhoneDirectoryList1) {
		
		for(PhoneDirectory group:groupPhoneDirectoryList) {
			if(group.getContactId()==phone.getContactId()) {
				temp=1;
				break;
			}
		}
		if(temp==1) {
		temp=0;
			logger.info("already in group"+phone);
		}
		else{
			notInGroupPhoneDirectoryList.add(phone);
		}
	}
	
		logger.info("Data get successfully !"+notInGroupPhoneDirectoryList.size());
return new ResponseEntity<List<PhoneDirectory>>(notInGroupPhoneDirectoryList,HttpStatus.OK);
}



}

