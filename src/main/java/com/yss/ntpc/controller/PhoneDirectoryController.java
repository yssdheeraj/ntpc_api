package com.yss.ntpc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.ntpc.entity.DepartmentEntity;
import com.yss.ntpc.entity.PhoneDirectory;
import com.yss.ntpc.entity.UserEntity;
import com.yss.ntpc.generic.service.IGenericService;

@RestController
@CrossOrigin
public class PhoneDirectoryController {
	
private static final Logger logger = LoggerFactory.getLogger(PhoneDirectoryController.class);
	
	@Autowired
	private IGenericService<PhoneDirectory> phoneService;
	
	@Autowired
	private IGenericService<UserEntity> userService; 

	@RequestMapping("/phoneDirectorylist")
	public  ResponseEntity<List<PhoneDirectory>> getPhoneDirectoryData() {
		
		List<PhoneDirectory> phoneDirectoryList=phoneService.fetch(new PhoneDirectory(),"WHERE flag  = false");
		
		if(phoneDirectoryList ==null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		else {
		return new  ResponseEntity(phoneDirectoryList,HttpStatus.OK);
		}
	}
	
	
	@PostMapping("/createPhoneDirectory")
	public ResponseEntity<?> savePhoneDirectory(@RequestBody PhoneDirectory objPhoneDirectory) {
		
		phoneService.save(objPhoneDirectory);
		
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping("/updatePhoneDirectory")
	public ResponseEntity<?> updatePhoneDirectory(@RequestBody PhoneDirectory objPhoneDirectory) {
		phoneService.update(objPhoneDirectory);
	logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}



	@PostMapping("/findPhoneDirectory/id")
	public ResponseEntity<?> findByPhoneDirectoryId(@RequestBody PhoneDirectory objPhoneDirectory) {
		
		PhoneDirectory PhoneDirectory=phoneService.find(objPhoneDirectory,objPhoneDirectory.getContactId());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(PhoneDirectory,HttpStatus.CREATED);
	}
	
	@PostMapping("/findPhoneDirectory/name")
	public ResponseEntity<?> findByPhoneDirectoryame(@RequestBody PhoneDirectory objPhoneDirectory) {
		
		PhoneDirectory PhoneDirectory=phoneService.find(objPhoneDirectory,"where name = '" +objPhoneDirectory.getName()+"'");
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(PhoneDirectory,HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/phoneDirectory/{contactId}" , method=RequestMethod.DELETE)
	public ResponseEntity<?> deletePhoneDirectory(@PathVariable ("contactId") long contactId){
		
		PhoneDirectory phoneDirectory = phoneService.find(new PhoneDirectory(), contactId);
		
		if (phoneDirectory == null) {
			logger.info("########PhoneDirectory with Id " +phoneDirectory + " Does Not Exist########");
			return new ResponseEntity<>(HttpStatus.FOUND);
		} else {
			phoneDirectory.setFlag(true);
			phoneService.update(phoneDirectory);
			logger.info("########PhoneDirectory with Id " +phoneDirectory + " Successfully Deleted########");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
	}
	
}
