package com.yss.ntpc.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yss.ntpc.entity.ReportEntity;
import com.yss.ntpc.generic.service.IGenericService;



@RestController
@CrossOrigin
public class ReportController {

	
	private static final Logger logger = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	private IGenericService<ReportEntity> reportEntitySerivce;
	
	@RequestMapping(path="/all/Report")
	public ResponseEntity<List<ReportEntity>> getAllReport(){
		
		try {
			List<ReportEntity> reportList = reportEntitySerivce.fetch(new ReportEntity());
			if (reportList.isEmpty()) {
				logger.info("#########Report List Does not Exist.....");
				return new ResponseEntity<List<ReportEntity>>(HttpStatus.NO_CONTENT);
			} else {
				logger.info("----Found " +reportList.size()+ " Report----");
				logger.info("----Report in Array----" +Arrays.toString(reportList.toArray()));
				return new ResponseEntity<List<ReportEntity>>(reportList,HttpStatus.OK);				
			}
			
			
			

		} catch (Exception e) {

			System.out.println("----Catch Exception in All Report----" +e.getMessage());
			return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
		}
	}
	
	
	
	@RequestMapping(path="/report/filter" ,produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<ReportEntity>> filterReport(@RequestBody ReportEntity reportEntity){
	
		logger.info("Return Report Entity " +getFilterQuery(reportEntity));
	List<ReportEntity> filterList = reportEntitySerivce.findAllByCondition(new ReportEntity(),getFilterQuery(reportEntity));
	
	logger.info("-----Fetch List-----" + filterList);
	
	if (filterList == null) {
		System.out.println("Null Data in  Filter List");
		return new ResponseEntity<List<ReportEntity>>(HttpStatus.NO_CONTENT);
	} 
	if(filterList.size() == 0) {
		System.out.println("No Data in Filter List");
			return new ResponseEntity<List<ReportEntity>>(HttpStatus.NO_CONTENT);
		}
		System.out.println("Data in  Filter List");
		return new ResponseEntity<List<ReportEntity>>(filterList,HttpStatus.OK);
	}


	private String getFilterQuery(ReportEntity reportEntity) {
		
		String query = "";
		
		if(!StringUtils.isEmpty(reportEntity.getMobile())) {
			query = query + "where mobile = '"+reportEntity.getMobile()+"' ";
		}
		
//		if(!StringUtils.isEmpty(reportEntity.getDepartmentName())) {
//			query = query +"where departmentName = '"+reportEntity.getDepartmentName()+"'";
//		}
		return query;
	}
	
}
