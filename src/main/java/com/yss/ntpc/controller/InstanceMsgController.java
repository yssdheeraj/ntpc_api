package com.yss.ntpc.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yss.ntpc.entity.InstanceMessageDetails;
import com.yss.ntpc.entity.InstanceMsgEntity;
import com.yss.ntpc.entity.PhoneDirectory;
import com.yss.ntpc.generic.service.IGenericService;
import com.yss.ntpc.services.InstanceMessageService;

@RestController
@CrossOrigin
public class InstanceMsgController {

	private static final Logger logger = LoggerFactory.getLogger(InstanceMsgController.class);
	@Autowired
	private IGenericService<InstanceMessageDetails> iDataGenericService;
	

	@Autowired
	private InstanceMessageService instanceMessageService;
	

	@RequestMapping("/instancemsglist")
	public  ResponseEntity<List<InstanceMessageDetails>> getInstanceMsgData() {

		logger.info("woriking");
		 List<InstanceMessageDetails> InstanceMsgList=iDataGenericService.fetch(new InstanceMessageDetails(),"WHERE flag  = false");
		return new ResponseEntity<List<InstanceMessageDetails>>(InstanceMsgList,HttpStatus.OK);
	}
	
	
	
	
	@PostMapping("/savemessageforsms")
	public ResponseEntity<?> saveInstacesMesssage(@RequestBody  InstanceMsgEntity instanceMsgEntity){
		
		logger.info("groups........................."+instanceMsgEntity);
		instanceMessageService.saveInstaceMessage(instanceMsgEntity);
		
		//RunBatchFile();
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
//	@PostMapping("/saveschedulemessage")
//	public ResponseEntity<?> saveschedulemessage(@RequestBody  InstanceMsgEntity instanceMsgEntity){
//		
//		logger.info("groups........................."+instanceMsgEntity);
//		instanceMessageService.saveScheduleMessage(instanceMsgEntity);
//		return new ResponseEntity<>(HttpStatus.OK);
//	}

//	@PostMapping("/createinstancemsg")
//	public ResponseEntity<?> saveInstanceMsg(@RequestBody InstanceMsgEntity objInstanceMsg) {
//	
//	iDataGenericService.save(objInstanceMsg);
//		logger.info("Data saved successfully !"+objInstanceMsg);
//		return new ResponseEntity<>(HttpStatus.CREATED);
//	}
//
//	@PutMapping("/updateinstancemsg")
//	public ResponseEntity<?> updateInstanceMsg(@RequestBody InstanceMsgEntity objInstanceMsg) {
//		iDataGenericService.update(objInstanceMsg);
//		logger.info("Data update successfully !");
//		return new ResponseEntity<>(HttpStatus.CREATED);
//	}
//
//	@DeleteMapping("/deleteinstancemsg")
//	public ResponseEntity<?> deleteInstanceMsg(@RequestParam(value = "InstanceMsgId", required = true) long InstanceMsgId) {
//		
//		logger.info("InstanceMsg id "+InstanceMsgId);
//		InstanceMsgEntity objInstanceMsg = iDataGenericService.find(new InstanceMsgEntity(), "where InstanceMsgId = " +  Long.valueOf(InstanceMsgId));
//		if (objInstanceMsg == null) {
//			return new ResponseEntity<>(objInstanceMsg, HttpStatus.NO_CONTENT);
//		}
//		
//		iDataGenericService.delete(objInstanceMsg);
//		logger.info("Data deleted successfully !");
//		return new ResponseEntity<>(HttpStatus.GONE);
//	}

//	@PostMapping("/findinstancemsg/id")
//	public ResponseEntity<?> findByInstanceMsgId(@RequestBody InstanceMsgEntity objInstanceMsg) {
//
//		InstanceMsgEntity InstanceMsg = iDataGenericService.find(objInstanceMsg, objInstanceMsg.getId());
//		logger.info("Data saved successfully !");
//		return new ResponseEntity<>(InstanceMsg, HttpStatus.CREATED);
//	}

	
	public String RunBatchFile() {
		
		logger.info("Message Sending Start");
		Runtime runtime = Runtime.getRuntime();
		try {
		    Process p1 = runtime.exec("cmd /c start D:\\NTPC_ALERT_SERVICES\\ntpc_api\\sms.bat");
		    InputStream is = p1.getInputStream();
		    int i = 0;
		    while( (i = is.read() ) != -1) {
		        System.out.print((char)i);
		    }
		} catch(IOException ioException) {
		    System.out.println(ioException.getMessage());
		}
	
		return "Success";
	}
	
}
