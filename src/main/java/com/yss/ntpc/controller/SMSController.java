package com.yss.ntpc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.yss.ntpc.entity.SMSEntity;
import com.yss.ntpc.generic.service.IGenericService;

@RestController
@CrossOrigin
public class SMSController {

	private static final Logger logger = LoggerFactory.getLogger(SMSController.class);
	@Autowired
	private IGenericService<SMSEntity> iDataGenericService;

	
	@RequestMapping("/smslist")
	public  ResponseEntity<List<SMSEntity>> getTextSMSData() {

		logger.info("woriking");
		 List<SMSEntity> TextSMSList=iDataGenericService.fetch(new SMSEntity(),"WHERE flag  = false");
		return new ResponseEntity<List<SMSEntity>>(TextSMSList,HttpStatus.OK);
	}
	
	

	@PostMapping("/createsms")
	public ResponseEntity<?> saveTextSMS(@RequestBody SMSEntity objTextSMS) {
	
	iDataGenericService.save(objTextSMS);
		logger.info("Data saved successfully !"+objTextSMS);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping("/updatesms")
	public ResponseEntity<?> updateTextSMS(@RequestBody SMSEntity objTextSMS) {
		iDataGenericService.update(objTextSMS);
		logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}


	@RequestMapping(value = "/deletesms/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteTextSMS(@PathVariable("id") long id) {
		
		logger.info("TextSMS id "+id);
		SMSEntity objTextSMS = iDataGenericService.find(new SMSEntity(),id);
		if (objTextSMS == null) {
			logger.info("no data found !");
			return new ResponseEntity<>(objTextSMS, HttpStatus.NO_CONTENT);
		}
		else
		{
			objTextSMS.setFlag(true);
			iDataGenericService.update(objTextSMS);
			logger.info("Data deleted successfully !");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		
	}

	@PostMapping("/findsms/id")
	public ResponseEntity<?> findByTextSMSId(@RequestBody SMSEntity objSMS) {

		SMSEntity TextSMS = iDataGenericService.find(objSMS, objSMS.getId());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(TextSMS, HttpStatus.CREATED);
	}

	@PostMapping("/findsms/name")
	public ResponseEntity<?> findByTextSMSame(@RequestBody SMSEntity objTextSMS) {

		SMSEntity TextSMS = iDataGenericService.find(objTextSMS, objTextSMS.getSmsName());

		logger.info("Data saved successfully !");
		return new ResponseEntity<>(TextSMS, HttpStatus.CREATED);
	}

}
