package com.yss.ntpc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yss.ntpc.entity.ContactGroup;
import com.yss.ntpc.generic.service.IGenericService;

@RestController
@CrossOrigin
public class ContactGroupController {

	private static final Logger logger = LoggerFactory.getLogger(ContactGroupController.class);
	
	@Autowired
	private IGenericService<ContactGroup> contactGroupService;
	
	
	@PostMapping("/createGroup")
	public ResponseEntity<?> saveGroup(@RequestBody ContactGroup objContactGroup) {
	
	contactGroupService.save(objContactGroup);
		logger.info("Data saved successfully !"+objContactGroup);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@PostMapping("/findContactGroup/name")
	public ResponseEntity<?> findByContactGroupName(@RequestBody ContactGroup objContactGroup) {
		
		ContactGroup ContactGroup=contactGroupService.find(objContactGroup,"where contactGroupName = '" +objContactGroup.getContactGroupName()+"'");
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(ContactGroup,HttpStatus.CREATED);
	}
	
	@RequestMapping("/grouplist")
	public  ResponseEntity<List<ContactGroup>> getContactGroupData() {

		logger.info("Data Retrive successfully");
		 List<ContactGroup> groupList=contactGroupService.fetch(new ContactGroup(),"WHERE flag  = false");
		return new ResponseEntity<List<ContactGroup>>(groupList,HttpStatus.OK);
	}
	
	@RequestMapping(value="/contactGroup/{contact_groupId}" , method=RequestMethod.DELETE)
	public ResponseEntity<?> deleteContactGroup(@PathVariable ("contact_groupId") long contact_groupId){
		
		ContactGroup contactGroup = contactGroupService.find(new ContactGroup(), contact_groupId);
		
		if (contactGroup == null) {
			logger.info("########ContactGroup with Id " +contactGroup + " Does Not Exist########");
			return new ResponseEntity<>(HttpStatus.FOUND);
		} else {
			contactGroup.setFlag(true);
			contactGroupService.update(contactGroup);
			logger.info("########ContactGroup with Id " +contactGroup + " Successfully Deleted########");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
	}

	
	@PutMapping(value="/updateGroup")
	public ResponseEntity<?> updatecontactGroup(@RequestBody ContactGroup objcontactGroup){
				contactGroupService.update(objcontactGroup);
		logger.info("Contact Group Updated Successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
}
