package com.yss.ntpc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.yss.ntpc.entity.TestDemo;
import com.yss.ntpc.generic.service.IGenericService;

@RestController
public class TestController {
	
	private static final Logger logger = LoggerFactory.getLogger(TestController.class);
	
	@Autowired
	private IGenericService<TestDemo> iDataGenericService;

	@RequestMapping("/all")
	public List<TestDemo> getTestData() {
		
		
		return iDataGenericService.fetch(new TestDemo());
	}
	
	@PostMapping("/save")
	public ResponseEntity<?> saveTest1(@RequestBody TestDemo objTest1) {
		iDataGenericService.save(objTest1);
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping("/update")
	public ResponseEntity<?> updateTest1(@RequestBody TestDemo objTest1) {
		iDataGenericService.update(objTest1);
	logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping("/delete")
	public ResponseEntity<?> deleteTest1(@RequestParam(value = "id", required = true) String id) {
		TestDemo objTest1 = iDataGenericService.find(new TestDemo(), "where id = " + id);
		if (objTest1 == null) {
			return new ResponseEntity<>(objTest1, HttpStatus.NO_CONTENT);
		}
		iDataGenericService.delete(objTest1);
logger.info("Data deleted successfully !");
		return new ResponseEntity<>(HttpStatus.GONE);
	}

	
	}
