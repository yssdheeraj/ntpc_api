package com.yss.ntpc.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yss.ntpc.entity.UserEntity;
import com.yss.ntpc.generic.service.IGenericService;
import com.yss.ntpc.services.PasswordEncription;

@RestController
@CrossOrigin
public class UserController {
	

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private IGenericService<UserEntity> iDataGenericService;

	

	@RequestMapping("/userlist")
	public  ResponseEntity<List<UserEntity>> getUserData() {

		logger.info("woriking");
		 List<UserEntity> userList=iDataGenericService.fetch(new UserEntity(),"WHERE flag  = false");
		 if(userList==null)
		 {
			 
		 }
		return new ResponseEntity<List<UserEntity>>(userList,HttpStatus.OK);
	}
	
	

	@PostMapping("/createuser")
	public ResponseEntity<?> saveUser(@RequestBody UserEntity objUser) {
		
		objUser.getPassword();
		objUser.setPassword(PasswordEncription.generateHash(objUser.getPassword()));
		
	iDataGenericService.save(objUser);
		logger.info("Data saved successfully !"+objUser);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping("/updateuser")
	public ResponseEntity<?> updateUser(@RequestBody UserEntity objUser) {
		
		iDataGenericService.update(objUser);
		logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.OK);
	}

	
	@RequestMapping(value = "/deleteuser/{loginId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@PathVariable ("loginId") String loginId) {
		logger.info("user id "+loginId);
		UserEntity objUser = iDataGenericService.find(new UserEntity(),"where loginId = '"+loginId+"'");
		if (objUser == null) {
			return new ResponseEntity<>(objUser, HttpStatus.NO_CONTENT);
		}
		else {
			objUser.setFlag(true);
			iDataGenericService.update(objUser);
			logger.info("Data deleted successfully !");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
	}

	@PostMapping("/findUser/id")
	public ResponseEntity<?> findByUserId(@RequestBody UserEntity objUser) {

		UserEntity User = iDataGenericService.find(objUser, objUser.getLoginId());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(User, HttpStatus.CREATED);
	}

	@PostMapping("/findUser/name")
	public ResponseEntity<?> findByUserame(@RequestBody UserEntity objUser) {

		UserEntity User = iDataGenericService.find(objUser, objUser.getUserName());

		logger.info("Data saved successfully !");
		return new ResponseEntity<>(User, HttpStatus.CREATED);
	}

	
	
	
}
