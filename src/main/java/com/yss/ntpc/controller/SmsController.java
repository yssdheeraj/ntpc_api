package com.yss.ntpc.controller;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.yss.ntpc.entity.InstanceMessageDetails;
import com.yss.ntpc.entity.InstanceMsgEntity;
import com.yss.ntpc.entity.PhoneDirectory;
import com.yss.ntpc.entity.SMSEntity;
import com.yss.ntpc.entity.ScheduleMessageEntity;
import com.yss.ntpc.entity.UserEntity;
import com.yss.ntpc.generic.service.IGenericService;
import com.yss.ntpc.services.InstanceMessageService;
import com.yss.ntpc.services.SendScheduledMessageSerivce;


@RestController
@CrossOrigin
public class SmsController {

	private static final Logger logger = LoggerFactory.getLogger(SmsController.class);
	@Autowired
	private IGenericService<SMSEntity> iDataGenericService;

	@Autowired
	private IGenericService<UserEntity> iDataGenericuserService;

	
	@Autowired
	private IGenericService<InstanceMessageDetails> instanceMessageDetailsService;
	
	@Autowired
	private IGenericService<ScheduleMessageEntity> scheduleMessageEntityService;
	
	@Autowired
	private InstanceMessageService instanceMessageService;
	
	@Autowired 
	private SendScheduledMessageSerivce sendScheduledMessageSerivce;
	
	
//	@GetMapping("/setDemo")
//	public ResponseEntity<?> ddddd(){
//		
//		sendScheduledMessageSerivce.findAllSmsByDate();
//		return new ResponseEntity<>(HttpStatus.OK);
//	}
	
	
	
	@PostMapping("/saveschedulemessage")
	public ResponseEntity<List<PhoneDirectory>> setScheduleMessage(@RequestBody InstanceMsgEntity objinstanceMsgEntity) throws ParseException{
		
			logger.info("----Came Message For Scheduled----" +objinstanceMsgEntity);
					objinstanceMsgEntity.setFlag(true);
					instanceMessageService.saveScheduledMessage(objinstanceMsgEntity);
					List<PhoneDirectory> dupicate =instanceMessageService.getDuplicatePhoneDirectory();
					 sendScheduledMessageSerivce.findAllSmsByDate();
					return new ResponseEntity<>(dupicate,HttpStatus.OK);
	}
	
	@RequestMapping("/smslist")
	public  ResponseEntity<List<SMSEntity>> getTextSMSData() {

		logger.info("woriking");
		 List<SMSEntity> TextSMSList=iDataGenericService.fetch(new SMSEntity(),"WHERE flag  = false");
		return new ResponseEntity<List<SMSEntity>>(TextSMSList,HttpStatus.OK);
	}
	
	
	@RequestMapping("/voicesmslist")
	public  ResponseEntity<List<SMSEntity>> getVoiceSMSData() {

		logger.info("woriking vvvvvvvvvvvvvv");
		 List<SMSEntity> sMSEntity=iDataGenericService.fetch(new SMSEntity(),"WHERE audioSms!= null AND flag  = false");
		return new ResponseEntity<List<SMSEntity>>(sMSEntity,HttpStatus.OK);
	}

	@PostMapping("/createsms")
	public ResponseEntity<?> saveTextSMS(@RequestParam(value="smsName") String smsName ,
			@RequestParam(value="smsDesc") String smsDesc ,
			@RequestParam(value="loginId") String loginId,
			@RequestParam(value="uploadFile",required=false) MultipartFile uploadFile) {
	System.out.println("****************************************************************");
		logger.info("desc "+smsDesc);
		logger.info("id "+loginId);
		logger.info("sms "+smsName);
		
		SMSEntity sms=new SMSEntity();
		if(uploadFile !=null) {
			if(!uploadFile.isEmpty()) {
				logger.info("file "+uploadFile.getOriginalFilename());
				String s = uploadFile.getOriginalFilename();
				File file=new File(s);
				sms.setFileName(uploadFile.getOriginalFilename());
				sms.setAudioSms(file);
			}
		}
		
		
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
		sms.setCreateDate(sdf.format(new Date()));

		
		sms.setSmsName(smsName);
		sms.setSmsDesc(smsDesc);
		UserEntity user=iDataGenericuserService.find(new UserEntity(),loginId);
		sms.setCreatedBy(user);
		iDataGenericService.save(sms);
		logger.info("Data saved successfully !"+sms);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping("/updatesms")
	public ResponseEntity<?> updateTextSMS(@RequestBody SMSEntity objTextSMS) {
		iDataGenericService.update(objTextSMS);
		logger.info("Data update successfully !");
		return new ResponseEntity<>(HttpStatus.CREATED);
	}


	@RequestMapping(value = "/deletesms/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteTextSMS(@PathVariable("id") long id) {
		
		logger.info("TextSMS id "+id);
		SMSEntity objTextSMS = iDataGenericService.find(new SMSEntity(),id);
		if (objTextSMS == null) {
			logger.info("no data found !");
			return new ResponseEntity<>(objTextSMS, HttpStatus.NO_CONTENT);
		}
		else
		{
			objTextSMS.setFlag(true);
			iDataGenericService.update(objTextSMS);
			logger.info("Data deleted successfully !");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		
	}

	@PostMapping("/findsms/id")
	public ResponseEntity<?> findByTextSMSId(@RequestBody SMSEntity objSMS) {

		SMSEntity TextSMS = iDataGenericService.find(objSMS, objSMS.getId());
		logger.info("Data saved successfully !");
		return new ResponseEntity<>(TextSMS, HttpStatus.CREATED);
	}

	@PostMapping("/findsms/name")
	public ResponseEntity<?> findByTextSMSame(@RequestBody SMSEntity objTextSMS) {

		SMSEntity TextSMS = iDataGenericService.find(objTextSMS, objTextSMS.getSmsName());

		logger.info("Data saved successfully !");
		return new ResponseEntity<>(TextSMS, HttpStatus.CREATED);
	}

}

