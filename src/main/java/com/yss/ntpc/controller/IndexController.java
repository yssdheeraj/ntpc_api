package com.yss.ntpc.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;

import com.yss.ntpc.entity.ChangePasswordEntity;
import com.yss.ntpc.entity.LoginDetailsEntity;
import com.yss.ntpc.entity.UserEntity;
import com.yss.ntpc.generic.service.IGenericService;
import com.yss.ntpc.services.PasswordEncription;


@RestController
@CrossOrigin
public class IndexController {

	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);
	
	@Autowired
	private IGenericService<UserEntity> userService;
	
	public static List<UserEntity> isUserLogin=new ArrayList();
	
	public static List<LoginDetailsEntity> userLoginDetails=new ArrayList();

	@Autowired
	private IGenericService<LoginDetailsEntity> logService;

	

	
	@PostMapping(path="/user/login")
	public ResponseEntity<?> doLogin(@RequestBody UserEntity objUser) throws Exception{
		
		logger.info("Do Login : " +objUser);
		
		objUser.setPassword(PasswordEncription.generateHash(objUser.getPassword()));
		
		String query = "where loginId ='"+objUser.getLoginId()+"' and password ='"+objUser.getPassword()+"' and flag  = false";
	
		 UserEntity dentity = userService.find(new UserEntity() ,query);
			
			logger.info("Do Login : " +dentity);
		 if (dentity !=null  ) {
		 	LoginDetailsEntity log = new LoginDetailsEntity();
		 	log.setLoginid(dentity.getLoginId());
		 	log.setPassword(dentity.getPassword());
			log.setLogin_datetime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()).toString());
			log.setLocationname(dentity.getLocation().getLocationName());
			logService.save(log);
			
			crearteSessionUser(log);	
			
				return new ResponseEntity<>(dentity,HttpStatus.OK);
		}
		
		else {
			System.out.println("error");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		 
	}
	
	
	
	
		 @PostMapping(path="/user/logout")
		 public ResponseEntity<?> logOut(@RequestBody UserEntity objUser) throws Exception{
			
				logger.info("Do Logout : ");
				for(LoginDetailsEntity user:userLoginDetails) {
					if(user.getLoginid().equals(objUser.getLoginId()))
					{
						user.setLogout_datetime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()).toString());
						userLoginDetails.remove(user);
						logService.update(user);
						logger.info("----------- log session  delete --------------"+userLoginDetails.size());
						
						logger.info("succesfully logout");
						break;
						
						
					}
				}
				return new ResponseEntity<>(HttpStatus.OK);					
		}
				
//		logger.info("LocationName :" +dentity.getLocation().getLocationName());
//		
//		logger.info("User LoinId :" +uentity.getLoginId());
//		logger.info("DB LoginId :" +dentity.getLoginId());
//	
//		logger.info("DB Password :" +dentity.getPassword());
//		logger.info("Entered Password " +uentity.getPassword());
//	
//		
//		
//		if (!dentity.getLoginId().equals(uentity.getLoginId())) {
//			logger.info("#########You Have Entered Wrong LoginId######### " +uentity.getLoginId());
//			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//		}
//		
//		else {
//		
//			if (!dentity.getPassword().equals(uentity.getPassword())) {
//				
//				logger.info("#########You Have Entered Wrong Password######### " +uentity.getPassword());
//				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//				
//			} else {
//			
//				logger.info("#########You Have Entered Right Password######### " +uentity.getPassword());
//				LoginDetailsEntity log = new LoginDetailsEntity();
//				log.setLoginid(dentity.getLoginId());
//				log.setPassword(dentity.getPassword());
//				log.setLogin_datetime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()).toString());
//				log.setLocationname(dentity.getLocation().getLocationName());
//				logService.save(log);
//				return new ResponseEntity<>(HttpStatus.OK);
//			}
//			
//		}
	
	
	
	 
	 @PostMapping(path="/password/reset")
	 public ResponseEntity<?> updatePassword(@RequestBody ChangePasswordEntity pentity){
		 
	 logger.info("Password Reset Successfully For.########## " + pentity.getLoginId());
		 UserEntity userEntity = userService.find(new UserEntity(),"where loginId= '"+pentity.getLoginId()+"'");
		 logger.info("user "+userEntity);
		 
		 userEntity.setPassword(pentity.getPassword());
		 userEntity.setPassword(PasswordEncription.generateHash(pentity.getPassword()));
		  userService.update(userEntity);
	
		 return new ResponseEntity<>(HttpStatus.OK);
	 }
	 
	 
	 
	 @PostMapping(path="/password/change")
	 public ResponseEntity<?> changePassword(@RequestBody ChangePasswordEntity pentity){
		 boolean result=false;
	 logger.info("Password Reset Successfully For.########## " + pentity);
	 String oldPwd=PasswordEncription.generateHash(pentity.getOldpassword());
	 
	 UserEntity userEntity = userService.find(new UserEntity(),"where loginId= '"+pentity.getLoginId()+"'");
		 logger.info("user "+userEntity);
	 if(userEntity.getPassword().equals(oldPwd)) {
		 logger.info("password match..............");
	 
		// userEntity.setPassword(pentity.getPassword());
		 userEntity.setPassword(PasswordEncription.generateHash(pentity.getPassword()));
		  userService.update(userEntity);
		  result=true;
		  return new ResponseEntity<>(result,HttpStatus.OK); 
	 }
	 else {
		 logger.info("password match. not .............");
		 
		 
		 result=false;
		 return new ResponseEntity<>(result,HttpStatus.NO_CONTENT);   
	 }
	
	 }
	 
	 
	 
	 
	 
	 //create session 
	 public List<UserEntity> crearteSessionUser(LoginDetailsEntity loginUser) {
		 
		 String query = "where loginid ='"+loginUser.getLoginid()+"' and login_datetime ='"+loginUser.getLogin_datetime()+"'";
			logger.info(query);
			loginUser = logService.find(new LoginDetailsEntity() ,query);
			
			userLoginDetails.add(loginUser);
			
		 UserEntity userEntity = userService.find(new UserEntity(),"where loginId= '"+loginUser.getLoginid()+"'");
		 isUserLogin.add(userEntity);
		 logger.info("---------- SESSION CREAT ----------------------"+isUserLogin.get(0).getLoginId());
		return  isUserLogin;
	 }
	 
	 
	 //destroy session 
	 public  List<UserEntity> destroySessionUser(UserEntity objUser) {
				 
		 isUserLogin.remove(0);
		 logger.info(objUser.getLoginId()+"---------- SESSION DESTROY ----------------------"+isUserLogin);
		return  isUserLogin;
	 }
	 
	 @RequestMapping("/loginstatus")
	 public ResponseEntity<?> getLoginStatus() {
		 logger.info("log----------"+isUserLogin);
		 try {
			 UserEntity userEntity=isUserLogin.get(0);
			 return new ResponseEntity<>(userEntity,HttpStatus.OK);
		 }catch(Exception e) {
			 UserEntity userEntity=null;
			 return new ResponseEntity<>( userEntity,HttpStatus.BAD_REQUEST);
		 }
		 
		 }
		
	
	 
	 @GetMapping("/all/loginDetails")
	 public ResponseEntity<?> getLoginDetails(){
		 
		 try {
			 List<LoginDetailsEntity> logList = logService.fetch(new LoginDetailsEntity());
			 
			 if (logList.isEmpty()) {
				 logger.info("Opps Error.....");
				 return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				int loginNumber = logList.size();
				logger.info("#########Found " +logList.size()+ " Department########");
				return new ResponseEntity<>(loginNumber,HttpStatus.OK);
			}
			
		 } catch (Exception e) {
			// TODO: handle exception
			 System.out.println(e.getMessage());
			 return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
		}
		 

		 
	 }
	 

}
