package com.yss.ntpc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.yss.ntpc.services.PasswordEncription;

@SpringBootApplication
@EnableScheduling
@ConfigurationProperties
//@EntityScan("com.yss.ntpc.entity.*")
//@ComponentScan({"com.yss.ntpc.services.*","com.yss.ntpc.generic.*"})
public class NtpcAlertServicesApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(NtpcAlertServicesApplication.class);	

	public static void main(String[] args) {
		
	
		SpringApplication.run(NtpcAlertServicesApplication.class, args);
		logger.info("#######################################################");
		logger.info("****-****-**** Application Started ****-****-****_****");
		logger.info("#######################################################");
		
		System.out.println("password"+PasswordEncription.generateHash("admin"));
	}
}
